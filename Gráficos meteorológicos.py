import pandas as pd
from matplotlib import pyplot as plt
try:  # Usamos para evitar un error al ingresar un código que no este en la base de datos

    codigo_observatorio=input('Ingrese el código de la estación que desea graficar: ').upper()
    nombre='homog_mo_'+codigo_observatorio+'.txt'  # Formación del nombre 
    
    if codigo_observatorio=='SAR'or codigo_observatorio=='JUN'or codigo_observatorio=='RAG': #Archivos con una sola variable (temperatura o precipitación)
        df = pd.read_fwf(nombre, skiprows=24) # Lectura del archivo txt 
        df['Year'] = df['Year'].astype(str)
        df['Month'] = df['Month'].astype(str)
        df["Fecha"] = df['Year']+ '-'+ df['Month'] # Creación de la columna fecha
        
        Fecha=pd.to_datetime(df['Fecha']) # Pasando de un dato string a una variable de tiempo.
        Col3=str(df.columns.values[2]) # Lectura de la columna a graficar(temperatura o precipitación)
        
        df['Media']=df[Col3].mean() # Creación de la columna con el dato de la media 
        # Grafica
        color=  {'Temperature':'red','Precipitation':'blue'}
        #%matplotlib qt 
        plt.ylabel(Col3,color=color[Col3])
        plt.xlabel('Fecha')
        plt.plot(Fecha,df[Col3],color=color[Col3],label=Col3)# Grafica los datos de la precipitación o de la temperatura
        color_media='dark'+color[Col3]
        plt.plot(Fecha,df['Media'],color=color_media,ls='dashed',linewidth=3)# Grafica la media
        plt.legend()
        
    else: # Para datos con dos variables
        df = pd.read_fwf(nombre, skiprows=27) #Lectura de datos(temperatura y precipitación)
        df['Year'] = df['Year'].astype(str)
        df['Month'] = df['Month'].astype(str)
        df["Fecha"] = df['Year']+ '-'+ df['Month']
        Fecha=pd.to_datetime(df['Fecha'])
        df['Media_temp']=df['Temperature'].mean() # Creación de la columna de la media de la temperatura
        df['Media_pre']=df['Precipitation'].mean() # Creación de la columna de la media de la precipitación 
        #%matplotlib qt
        #Grafica 
        fig,ax1=plt.subplots()
        color='tab:red'
        ax1.set_xlabel('                             Fecha')
        ax1.set_ylabel('Temperatura',color=color)
        ax1.plot(Fecha,df['Temperature'],color=color,label='Temperatura')# Grafica  los datos de la temperatura
        ax1.plot(Fecha,df['Media_temp'],color='darkred',ls='dashed', linewidth=3) # Grafica la media de la temperatura
        ax1.tick_params(axis='y',labelcolor=color)
        ax2=ax1.twinx() # Genera ejes que comparten un eje (y) pero tienen diferentes escalas
        color='tab:blue'
        ax2.set_ylabel('Precipitacion',color=color)
        ax2.plot(Fecha,df['Precipitation'],color=color,label='Precipitación')# Grafica los datos de la precipitación
        ax2.plot(Fecha,df['Media_pre'],color='darkblue',ls='dashed',linewidth=3) # Grafica la media de la precipitación
        ax2.tick_params(axis='y',labelcolor=color)
        fig.tight_layout()  # Ajuste de la grafica
        fig.legend(loc='lower left',ncol=2, fontsize=9) # ncol divide en dos columnas la leyenda
                                                        #fontsize tamaño de letra 
        
    # Creación del diccionario que contiene información para el titulo de la grafica
    diccionario={'ALT':'Altdorf|ALT|438msnm|Central Alpine north slope', 'ANT':'Andermatt|ANT|1438 msnm|Central Alpine north slope', 'RAG': 'Bad Ragaz|RAG|496msnm|Northern and central Grisons',
                 'BAS':'Basel/Binningen|BAS|316msnm|Eastern Jura', 'BER':'Bern/Zollikofen|BER|553msnm|Central plateau', 'CHM':'Chaumont|CHM|1136msnm|Western Jura',
                 'CHD':'Château-dOex|CHD|1028msnm|Western Alpine north slope','GSB':'Col du Grand St-Bernard|GSB|2472msnm|Alpine south side','DAV':'Davos|DAV|1594msnm|Northern and central Grisons',
                 'ELM':'Elm|ELM|958msnm|Eastern Alpine north slope', 'ENG':'Engelberg|ENG|1036msnm|Central Alpine north slope','GVE':'Genève/Cointrin|GVE|411msnm|Western plateau',
                 'GRH':'Grimsel Hospiz|GRH|1980msnm|Western Alpine north slope','GRC':'Grächen|GRC|1605msnm|Valais', 'JUN':'Jungfraujoch|JUN|3571msnm|Western Alpine north slope',
                 'CDF':'La Chaux-de-Fonds|CDF|1017msnm|Western Jura','OTL':'Locarno/Monti|OTL|367msnm|Alpine south side','LUG':'Lugano|LUG|273msnm|Alpine south side',
                 'LUZ':'Luzern|LUZ|454msnm|Central plateau',
                 'MER':'Meiringen|MER|589msnm|Western Alpine north slope', 'NEU': 'Neuchâtel|NEU|485msnm|Western plateau','PAY':'Payerne|PAY|490msnm|Western plateau', 'SBE': 'S. Bernardino|SBE|1639msnm|Alpine south side',
                 'SAM':'Samedan|SAM|1709msnm|Engadina', 'SAR':'Sargans|SAR|585msnm|Grisones septentrionales y centrales', 'SIA':'Segl-Maria|SIA|1804msnm|Engadina','SIO':'Sion|SIO|482msnm|Valais',
                 'STG':'St. Gallen|STG|776msnm|North-eastern platea','SAE':'Säntis|SAE|2501msnm|Eastern Alpine north slope','SMA': 'Zürich|SMA|556msnm|North-eastern plateau'
                 }
    plt.title(diccionario[codigo_observatorio] ,fontsize=13, fontweight='bold')
    plt.show()
except FileNotFoundError:
   print('El código que ingreso no se encuentra en la base de datos.')